package br.ucsal.bes20212.bd2.aula10.tui;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import br.ucsal.bes20212.bd2.aula10.domain.Correntista;
import br.ucsal.bes20212.bd2.aula10.persistence.CorrentistaDAO;
import br.ucsal.bes20212.bd2.aula10.persistence.DBUtil;

public class Exemplo {

	public static void main(String[] args) throws SQLException {
		try {
			DBUtil.connect("postgres", "abcd1234");
			// executarConsultaComScroll();
			inserirCorrentista();
			// listarCorrentistas();
		} finally {
			DBUtil.close();
		}
	}

	private static void inserirCorrentista() throws SQLException {
		Correntista correntista1 =new Correntista("Ester", "123123", 2010);
		CorrentistaDAO.persist(correntista1);
		System.out.println(correntista1);
	}

	private static void listarCorrentistas() throws SQLException {
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		System.out.println("Correntistas: ");
		correntistas.stream().forEach(System.out::println);
	}

	private static void executarConsultaComScroll() throws SQLException {
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = DBUtil.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE); ResultSet rs = stmt.executeQuery(query);) {
			rs.afterLast();
			while (rs.previous()) {
				String nome = rs.getString("nm");
				System.out.println("Nome=" + nome);
			}
		}
	}

}
