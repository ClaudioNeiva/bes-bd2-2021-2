-- Excluir tabelas, caso existam
drop table if exists veiculo cascade;
drop table if exists grupo cascade;
drop table if exists acessorio cascade;
drop table if exists acessorio_veiculo cascade;

-- Criar tabelas
create table veiculo (
	id						serial			not null,
	placa					char(7)			not null,
	ano_fabricacao			integer			not null,
	data_aquisicao			date			not null,
	data_venda				date			    null,
	quilometragem			integer			not null,
	codigo_grupo			smallint		not null,
	codigo_modelo			smallint		not null,
	constraint pk_veiculo
		primary key (id),
	constraint un_veiculo_placa
		unique (placa),
	constraint ck_veiculo_quilometragem
		check (quilometragem >= 0));
	
create table grupo (
	codigo					smallserial		not null,
	nome					varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));
	
create table acessorio (
	sigla					varchar(10)		not null,
	nome					varchar(40)		not null,
	constraint pk_acessorio
		primary key (sigla));
 
create table acessorio_veiculo (
	id_veiculo				integer			not null,
	sigla_acessorio			varchar(10)		not null,
	constraint pk_acessorio_veiculo
		primary key (id_veiculo, sigla_acessorio));
	
-- Para efeito de teste, tornamos o grupo opcional em veiculo
alter table veiculo
	alter column codigo_grupo drop not null;
	
-- Alterar tabelas, adicionando chaves estrangeiras
alter table veiculo
	drop constraint fk_veiculo_grupo;

alter table veiculo
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo
		on delete set null;
		
alter table acessorio_veiculo
	drop constraint fk_acessorio_veiculo_veiculo;

alter table acessorio_veiculo
	add constraint fk_acessorio_veiculo_veiculo
		foreign key (id_veiculo)
		references veiculo
		on delete cascade,
	add constraint fk_acessorio_veiculo_acessorio
		foreign key (sigla_acessorio)
		references acessorio
		on update cascade;
		
--		on update restrict
--		on delete restrict

insert into grupo (nome) values 
('Básico'),
('Utilitário'),
('Luxo');

select * 
from grupo;

delete 
from grupo;

-- insert de teste em veiculo, com modelo fixo, pois a tabela correspondente não foi criada ainda.
insert into veiculo (
	placa					,
	ano_fabricacao			,
	data_aquisicao			,
	data_venda				,
	quilometragem			,
	codigo_grupo			,
	codigo_modelo			)
values
(	'ABC1234',2000,'2021-07-01',null, 9000, (select codigo from grupo where nome = 'Luxo') ,1);

insert into  acessorio (sigla,nome) values
('ar','arcondicionado'),
('ve','vidro elétrico'),
('te','trave elétrica');

insert into acessorio_veiculo (id_veiculo, sigla_acessorio) values
( (select id from veiculo where placa = 'ABC1234'),'ar'),
( (select id from veiculo where placa = 'ABC1234'),'ve');

select * 
from veiculo;

select * 
from acessorio_veiculo;

select * 
from acessorio;

update acessorio
set sigla = 'ac'
where sigla = 'ar';

delete
from veiculo
where placa = 'ABC1234';

delete
from grupo
where nome = 'Luxo';

insert into acessorio_veiculo (id_veiculo, sigla_acessorio) values
( (select id from veiculo where placa = 'ABC1234'),'ac'),
( (select id from veiculo where placa = 'ABC1234'),'ve');

delete
from acessorio
where sigla = 've';
