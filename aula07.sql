----- Motivações para triggers!

-- Atributo derivado?

-- item_nota_fiscal = id_produto, quantidade, valor_unitario, desconto, VALOR_TOTAL
-- o valor_total não precisa ser armazenado, porque ele é derivado dos atributos quantidade, valor_unitario, desconto;
-- motivação para armazenar o VALOR_TOTAL?
select	quantidade * valor_unitario - desconto as valor_total	-- perda de performance, pois a cada execução teríamos o cálculo sendo feito
																-- complexidade das consultas, que deveria reproduzir essa contad, mas isso pode ser
																-- resolvido com o uso de views (não materializadas)
from	item_nota_fiscal .......

create view v_item_nota_fiscal
as
	select	id_produto,
			quantidade,
			valor_unitario,
			desconto,
			quantidade * valor_unitario - desconto as valor_total	
	from 	item_nota_fiscal
;	-- essa view resolve o problema da complexidade, mas não da performance!

-- Caso a opção seja criar o atributo VALOR_TOTAL, esse deve ser mantido (atualizado) pelo próprio sgbd, evitando que ocorra um
-- inconsistência entre o atributos derivado (VALOR_TOTAL) e seus componentes (quantidade, valor_unitario, desconto) / processo
-- de cálculo (quantidade * valor_unitario - desconto).
-- Em outras palavras, esse campo dev ser atualizado automaticamente (via programação no SGBD) quando um registro for atualizado
-- e/ou quando um dos atributos determinantes do valor total for modificado.

insert into item_nota_fiscal (id_produto, quantidade, valor_unitario, desconto, valor_total)
values (...)	-- se vc deixar o programado da camada de aplicação fornecer o valor_total, vc corre o risco de deixar o banco 
				-- em um estado inconsistente.

-- Falar das tabelas de saldo!!!!

create table item_nota_fiscal (
	id					serial			not null,
	nota_fiscal_id		integer			not null,		-- fica como dever de casa a criação da tabela de nota fiscal e de produto
	produto_id			integer			not null,
	quantidade			numeric(10,3)	not null,	
	valor_unitario		numeric(10,2)	not null,
	desconto			numeric(10,2)	not null,	
	valor_total			numeric(10,2)	not null,		-- atributo derivado - deriva da quantidade, valor unitário e desconto
	constraint pk_item_nota_fiscal
		primary key (id));

l_valor_local = quantidade * valor_unitario;  -- esquece de incluir o desconto!!!
insert into item_nota_fiscal (........., valor_total)
values (.........., :l_valor_local);
-- cadastro de nota: quantidade * valor_unitario - desconto
-- edição de nota: quantidade * valor_unitario
--

create or replace function f_trg_iu_item_nota_fiscal()
returns trigger
as $$
begin
	if TG_OP = 'UPDATE' then
		raise notice 'valor old=%', old;
	end if;
	new.valor_total = new.quantidade * new.valor_unitario - new.desconto;
	raise notice 'valor new=%', new;
	if (select count(*) from item_nota_fiscal) > 2 then
		raise exception 'Só posso vender até 3 itens.';
	end if;
	return new;
end;
$$ language PLPGSQL;

CREATE TRIGGER trg_iu_item_nota_fiscal
BEFORE insert OR update of quantidade, valor_unitario, desconto, valor_total
ON item_nota_fiscal 
FOR EACH ROW
EXECUTE PROCEDURE f_trg_iu_item_nota_fiscal();

insert into item_nota_fiscal (nota_fiscal_id, produto_id, quantidade, valor_unitario, desconto)
values 
(1,23, 200, 120, 1250);

select 	* 
from 	item_nota_fiscal;

update item_nota_fiscal
set valor_total = 2345345;

update item_nota_fiscal
set	valor_unitario = valor_unitario * 10.1;		-- de 0 a todos registros

NEW.id = null					
NEW.nota_fiscal_id = 1
NEW.produto_id = 30
NEW.quantidade = 1000
NEW.valor_unitario = 20		
NEW.desconto = 50			
NEW.valor_total	= 19950		

-------------------------------------

-- CENÁRIO PARA USO DE TRIGGERS

-- Controle de estoque

create table item(
	id			serial			not null,
	nome		varchar(30)		not null,
	constraint pk_item
		primary key (id));
	
create table movimento(
	id			serial			not null,
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_movimento
		primary key (id));	
		
create table saldo(								--- Por que controlar o saldo numa tabela se podemos derivar o saldo a partir dos dados do movimento?
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_saldo
		primary key (data, id_item));	
		
alter table movimento
	add constraint fk_movimento_item
		foreign key (id_item)
		references item;
alter table saldo
	add constraint fk_saldo_item
		foreign key (id_item)
		references item;		

INSERT INTO item (nome)
VALUES 
('Parafuso'),
('Porca'),
('Vela'),
('Bobina');

drop trigger if exists trg_i_movimento on movimento;
drop function if exists f_trg_i_movimento;

create or replace function f_trg_i_movimento()
returns trigger
as
$$
begin
	raise notice 'aqui vamos escrever a lógica para criação do saldo a partir do movimento!';
	raise notice 'data=%, id_item=%, qtd=%',new.data, new.id_item, new.qtd;
	-- insert em saldo??? ou update em saldo??? quais valores são inseridos/atualizados na tabela de saldos???
	return new;
end;
$$
language PLPGSQL;

create trigger trg_i_movimento
before insert
on movimento
for each row
execute procedure f_trg_i_movimento();

insert into movimento (data, id_item,qtd)
values
('2021-09-27',(select id from item where nome = 'Parafuso'),100);

-- Resultado esperado: na tabela de saldo
-- (data, id_item, qtd)
-- ('2021-09-27', 1, 100)

insert into movimento (data, id_item,qtd)
values
('2021-09-27',(select id from item where nome = 'Parafuso'),-8);

-- Resultado esperado: na tabela de saldo
-- (data, id_item, qtd)
-- ('2021-09-27', 1, 92)


delete 
from movimento;

select * from item;
select * from movimento;
select * from saldo;


