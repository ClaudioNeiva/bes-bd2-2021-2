package br.ucsal.bes20202.bd2.escola;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tab_aluno_externo")
//@DiscriminatorValue("AEX")
public class AlunoExterno extends Aluno {

	@Column(nullable = false)
	private String nomeEscolaOrigem;

	public AlunoExterno() {
		super();
	}

	public AlunoExterno(String nome, SituacaoAlunoEnum situacao, String nomeEscolaOrigem) {
		super(nome, situacao);
		this.nomeEscolaOrigem = nomeEscolaOrigem;
	}

	public String getNomeEscolaOrigem() {
		return nomeEscolaOrigem;
	}

	public void setNomeEscolaOrigem(String nomeEscolaOrigem) {
		this.nomeEscolaOrigem = nomeEscolaOrigem;
	}

	@Override
	public String toString() {
		return "AlunoExterno [nomeEscolaOrigem=" + nomeEscolaOrigem + ", toString()=" + super.toString() + "]";
	}
}
