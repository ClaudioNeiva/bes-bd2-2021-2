package br.ucsal.bes20202.bd2.escola;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_aluno", uniqueConstraints = { @UniqueConstraint(name = "un_aluno_cpf", columnNames = { "cpf" }),
		@UniqueConstraint(name = "un_aluno_rg", columnNames = { "num_rg", "orgaoExpedidor" }) })
@SequenceGenerator(name = "sq_aluno", sequenceName = "sq_aluno")
//@DiscriminatorColumn(name = "tipo", length = 3, discriminatorType = DiscriminatorType.STRING)
//@DiscriminatorValue("ALN")
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
public class Aluno {

	// int
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_aluno")
	private Integer matricula;

	// varchar(40)
	private String nome;

	// char(11)
	private String cpf;

	@ManyToOne
	private Curso curso;

	// char(3)
	// @Enumerated(EnumType.STRING)
	@Convert(converter = SituacaoAlunoConverter.class)
	private SituacaoAlunoEnum situacao;

	// numeric(10,2) - renda_familiar
	private BigDecimal rendaFamiliar;

	// num_rg
	@Column(name = "num_rg")
	private Long numRg;

	// orgao_expedidor
	private String orgaoExpedidor;

	@Transient
	private Uf ufOrgaoExpedidor;

	// cada telefone deve ser varchar(15)
	@ElementCollection
	private List<String> telefones;

	@Embedded
	private Endereco endereco;

	public Aluno() {
		super();
	}

	public Aluno(String nome, SituacaoAlunoEnum situacaoAlunoEnum) {
		this.nome = nome;
		this.situacao = situacaoAlunoEnum;
	}

	public Aluno(String nome, Curso curso, SituacaoAlunoEnum situacao) {
		this(nome, situacao);
		this.curso = curso;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getRendaFamiliar() {
		return rendaFamiliar;
	}

	public void setRendaFamiliar(BigDecimal rendaFamiliar) {
		this.rendaFamiliar = rendaFamiliar;
	}

	public Long getNumRg() {
		return numRg;
	}

	public void setNumRg(Long numRg) {
		this.numRg = numRg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Uf getUfOrgaoExpedidor() {
		return ufOrgaoExpedidor;
	}

	public void setUfOrgaoExpedidor(Uf ufOrgaoExpedidor) {
		this.ufOrgaoExpedidor = ufOrgaoExpedidor;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((curso == null) ? 0 : curso.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numRg == null) ? 0 : numRg.hashCode());
		result = prime * result + ((orgaoExpedidor == null) ? 0 : orgaoExpedidor.hashCode());
		result = prime * result + ((rendaFamiliar == null) ? 0 : rendaFamiliar.hashCode());
		result = prime * result + ((situacao == null) ? 0 : situacao.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		result = prime * result + ((ufOrgaoExpedidor == null) ? 0 : ufOrgaoExpedidor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (curso == null) {
			if (other.curso != null)
				return false;
		} else if (!curso.equals(other.curso))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numRg == null) {
			if (other.numRg != null)
				return false;
		} else if (!numRg.equals(other.numRg))
			return false;
		if (orgaoExpedidor == null) {
			if (other.orgaoExpedidor != null)
				return false;
		} else if (!orgaoExpedidor.equals(other.orgaoExpedidor))
			return false;
		if (rendaFamiliar == null) {
			if (other.rendaFamiliar != null)
				return false;
		} else if (!rendaFamiliar.equals(other.rendaFamiliar))
			return false;
		if (situacao != other.situacao)
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		if (ufOrgaoExpedidor == null) {
			if (other.ufOrgaoExpedidor != null)
				return false;
		} else if (!ufOrgaoExpedidor.equals(other.ufOrgaoExpedidor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", curso=" + curso + ", situacao="
				+ situacao + ", rendaFamiliar=" + rendaFamiliar + ", numRg=" + numRg + ", orgaoExpedidor="
				+ orgaoExpedidor + ", ufOrgaoExpedidor=" + ufOrgaoExpedidor + ", telefones=" + telefones + ", endereco="
				+ endereco + "]";
	}

}
