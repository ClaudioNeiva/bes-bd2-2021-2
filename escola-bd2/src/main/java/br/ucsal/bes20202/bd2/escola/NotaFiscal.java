package br.ucsal.bes20202.bd2.escola;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Where;

@Entity
public class NotaFiscal {

	@Id
	private Long id;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "notaFiscal", orphanRemoval = true)
	@Where(clause = "tipo = 'P'")
	private Set<ItemNotaFiscal> itensEstoqueProprio;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "notaFiscal", orphanRemoval = true)
	@Where(clause = "tipo = 'T'")
	private Set<ItemNotaFiscal> itensEstoqueTerceito;

	public NotaFiscal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<ItemNotaFiscal> getItensEstoqueProprio() {
		return itensEstoqueProprio;
	}

	public void setItensEstoqueProprio(Set<ItemNotaFiscal> itensEstoqueProprio) {
		this.itensEstoqueProprio = itensEstoqueProprio;
	}

	public Set<ItemNotaFiscal> getItensEstoqueTerceito() {
		return itensEstoqueTerceito;
	}

	public void setItensEstoqueTerceito(Set<ItemNotaFiscal> itensEstoqueTerceito) {
		this.itensEstoqueTerceito = itensEstoqueTerceito;
	}

}
