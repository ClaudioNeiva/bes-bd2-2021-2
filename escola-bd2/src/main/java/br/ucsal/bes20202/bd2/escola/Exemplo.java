package br.ucsal.bes20202.bd2.escola;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("escola");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Uf ufSP = new Uf("SP", "São Paulo");
		em.persist(ufSP);

		Cidade cidadeSAO = new Cidade(ufSP, "SAO", "São Paulo");
		Cidade cidadeGRU = new Cidade(ufSP, "GRU", "Guarulhos");
		Cidade cidadeSBC = new Cidade(ufSP, "SBC", "São Bernardo do Campo");
		em.persist(cidadeSAO);
		em.persist(cidadeGRU);
		em.persist(cidadeSBC);

		Curso cursoBES = new Curso("BES", "Bacharelado em Engenharia de Software", LocalDate.of(2014, 01, 05));
		em.persist(cursoBES);

		Aluno alunoClaudio = new Aluno("Claudio Neiva", SituacaoAlunoEnum.FORMADO);
		alunoClaudio.setEndereco(new Endereco("Rua 1", "123", "Pituaçu", cidadeSAO));
		alunoClaudio.setCurso(cursoBES);
		em.persist(alunoClaudio);

		AlunoExterno alunoPedro = new AlunoExterno("Pedro", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoPedro.setEndereco(new Endereco("Rua 2", "456", "Pituaçu", cidadeGRU));
		alunoPedro.setCurso(cursoBES);
		em.persist(alunoPedro);

		AlunoExterno alunoJoaquim = new AlunoExterno("Joaquim", SituacaoAlunoEnum.ATIVO, "UFBA");
		alunoJoaquim.setEndereco(new Endereco("Rua 3", "867", "Brotas", cidadeSAO));
		alunoJoaquim.setCurso(cursoBES);
		em.persist(alunoJoaquim);

		em.getTransaction().commit();

		em.clear();

		alunoClaudio = em.find(Aluno.class, alunoClaudio.getMatricula());
		alunoJoaquim = em.find(AlunoExterno.class, alunoJoaquim.getMatricula());

		em.close();
		emf.close();

	}

}
