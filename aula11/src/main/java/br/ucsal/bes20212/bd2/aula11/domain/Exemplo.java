package br.ucsal.bes20212.bd2.aula11.domain;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		Aluno aluno = new Aluno(10, "Claudio");

		em.getTransaction().begin();
		em.persist(aluno);
		// System.out.println("Tecle ENTER para continuar...");
		// scanner.nextLine();
		em.getTransaction().commit();

		em.getTransaction().begin();
		aluno.setNome("Maria");
		em.getTransaction().commit();

		// System.out.println("em.contains(aluno)=" + em.contains(aluno));
		emf.close();

	}
}
