-------------------------------------

-- CENÁRIO PARA USO DE TRIGGERS

-- Controle de estoque

create table item(
	id			serial			not null,
	nome		varchar(30)		not null,
	constraint pk_item
		primary key (id));
	
create table movimento(
	id			serial			not null,
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_movimento
		primary key (id));	
		
create table saldo(								--- Por que controlar o saldo numa tabela se podemos derivar o saldo a partir dos dados do movimento?
	data		date			not null,
	id_item		integer			not null,
	qtd			integer			not null,
	constraint pk_saldo
		primary key (data, id_item));	
		
alter table movimento
	add constraint fk_movimento_item
		foreign key (id_item)
		references item;
alter table saldo
	add constraint fk_saldo_item
		foreign key (id_item)
		references item;		

INSERT INTO item (nome)
VALUES 
('Parafuso'),
('Porca'),
('Vela'),
('Bobina');

drop function if exists f_trg_iud_movimento;

create or replace function f_trg_iud_movimento()
returns trigger
as
$$
declare 
	v_data_saldo_anterior		date;
	v_qtd_saldo_anterior		integer;
begin

	if TG_OP = 'DELETE' or TG_OP = 'UPDATE' then
	
		-- Atualizar os saldos para o item, a partir da data do movimento.
		update 	saldo
			set qtd = qtd - old.qtd		
		where	id_item  = old.id_item
		and		data	>= old.data;
	
	end if;

	if TG_OP = 'INSERT' or TG_OP = 'UPDATE' then

		-- Obter a data do saldo para o item na data do movimento, que pode ser um saldo na data ou em data anterior.
		select	max(s.data)				
		into	v_data_saldo_anterior
		from	saldo s
		where	s.id_item 	= new.id_item
		and		s.data	   <= new.data; 

		if  v_data_saldo_anterior is null then
			-- Não existe saldo nem na data do movimento nem em data anterior
			v_qtd_saldo_anterior = 0;
		else
			-- Existe saldo na data do movimento ou em data anterior.
			-- Obter o valor do saldo para o item na data do saldo_anterior.
			select		s.qtd
				into	v_qtd_saldo_anterior
				from	saldo s
				where	s.id_item 	= new.id_item
				and		s.data		= v_data_saldo_anterior;
		end if;

		if v_data_saldo_anterior is null or v_data_saldo_anterior <> new.data then
			-- Caso não exista saldo anterior ou o saldo anterior seja em data diferente da data do movimento.
			-- Criar saldo para o item na data do movimento, com a quantidade do saldo anterior.
			insert into saldo (data, id_item, qtd)
				values (new.data, new.id_item, v_qtd_saldo_anterior);
		end if;	

		-- Atualizar os saldos para o item, a partir da data do movimento.
		update 	saldo
			set qtd = qtd + new.qtd
		where	id_item  = new.id_item
		and		data	>= new.data;

	end if;

	if TG_OP = 'DELETE' then
		-- Para triggers do evento DELETE, é OBRIGATÓRIO o retorno do old.
		return old;
	else
		-- Para triggers dos eventos INSERT ou UPDATE, é OBRIGATÓRIO o retorno do new.
		return new;	
	end if;
end;
$$
language PLPGSQL;

drop trigger if exists trg_iud_movimento on movimento;

create trigger trg_iud_movimento
before insert or update or delete
on movimento
for each row
execute procedure f_trg_iud_movimento();

insert into movimento (data, id_item,qtd)
values
('2021-09-29',(select id from item where nome = 'Parafuso'),120);

insert into movimento (data, id_item,qtd)
values
('2021-09-27',(select id from item where nome = 'Vela'),5);

insert into movimento (data, id_item,qtd)
values
('2021-09-30',(select id from item where nome = 'Parafuso'),40);

insert into movimento (data, id_item,qtd)
values
('2021-09-30',(select id from item where nome = 'Parafuso'),20);

insert into movimento (data, id_item,qtd)
values
('2021-10-05',(select id from item where nome = 'Parafuso'),30);

insert into movimento (data, id_item,qtd)
values
('2021-08-30',(select id from item where nome = 'Parafuso'),15);

insert into movimento (data, id_item,qtd)
values
('2021-09-15',(select id from item where nome = 'Parafuso'),-10);

delete from movimento;
delete from saldo;

delete
from 	movimento
where 	id_item = (select id from item where nome = 'Parafuso')
and		data = '2021-09-29';

update	movimento
set		qtd = 100
where 	id_item = (select id from item where nome = 'Parafuso')
and		data = '2021-09-29';

select * from movimento;
select * from saldo order by id_item asc , data asc;

--"2021-08-30"	1	15
--"2021-09-15"	1	5
--"2021-09-29"	1	105					-- mudar de 120 para 100
--"2021-09-30"	1	165
--"2021-10-05"	1	195
--"2021-09-27"	3	5

-- Resultado esperado: na tabela de saldo
-- (data, id_item, qtd)
-- ('2021-09-27', 1, 100)

insert into movimento (data, id_item,qtd)
values
('2021-09-27',(select id from item where nome = 'Parafuso'),-8);

-- Resultado esperado: na tabela de saldo
-- (data, id_item, qtd)
-- ('2021-09-27', 1, 92)

delete 
from movimento;

select * from item;
select * from movimento;
select * from saldo;














create or replace function f_trg_iud_movimento()
returns trigger
as
$$
declare 
	v_data_saldo_anterior		date;
	v_qtd_saldo_anterior		integer;
begin

	if TG_OP = 'DELETE' or TG_OP = 'UPDATE' then
		saldo = f_atualizar_saldo(old.id_item, old.data, -1 * old.qtd);
	end if;

	if TG_OP = 'INSERT' or TG_OP = 'UPDATE' then
		v_data_saldo_anterior = f_obter_data_saldo_anterior(new.id_item, new.data);
		if  v_data_saldo_anterior is null then
			v_qtd_saldo_anterior = 0;
		else
			v_qtd_saldo_anterior = f_obter_valor_saldo_anterior(new.id_item, v_data_saldo_anterior);
		end if;
		if v_data_saldo_anterior is null or v_data_saldo_anterior <> new.data then
			f_inserir_saldo(new.id_item, new.data, v_qtd_saldo_anterior);
		end if;	
		saldo = f_atualizar_saldo(new.id_item, new.data, new.qtd);
	end if;

	if TG_OP = 'DELETE' then
		return old;
	else
		return new;	-- INSERT or UPDATE
	end if;
end;
$$
language PLPGSQL;

