-- Excluir tabelas, caso existam
drop table if exists veiculo cascade;
drop table if exists grupo cascade;
drop table if exists acessorio cascade;
drop table if exists acessorio_veiculo cascade;

-- Criar tabelas
create table veiculo (
	id						serial			not null,
	placa					char(7)			not null,
	ano_fabricacao			integer			not null,
	data_aquisicao			date			not null,
	data_venda				date			    null,
	quilometragem			integer			not null,
	codigo_grupo			smallint		not null,
	codigo_modelo			smallint		not null,
	constraint pk_veiculo
		primary key (id),
	constraint un_veiculo_placa
		unique (placa),
	constraint ck_veiculo_quilometragem
		check (quilometragem >= 0));
	
create table grupo (
	codigo					smallserial		not null,
	nome					varchar(40)		not null,
	constraint pk_grupo
		primary key (codigo));

create table modelo (
	codigo					smallserial		not null,
	nome					varchar(40)		not null,
	cnpj_fabricante			char(14)		    null,
	constraint pk_modelo
		primary key (codigo));
	
create table acessorio (
	sigla					varchar(10)		not null,
	nome					varchar(40)		not null,
	constraint pk_acessorio
		primary key (sigla));
 
create table acessorio_veiculo (
	id_veiculo				integer			not null,
	sigla_acessorio			varchar(10)		not null,
	constraint pk_acessorio_veiculo
		primary key (id_veiculo, sigla_acessorio));
	
-- Para efeito de teste, tornamos o grupo opcional em veiculo
alter table veiculo
	alter column codigo_grupo drop not null;
	
-- Alterar tabelas, adicionando chaves estrangeiras
alter table veiculo
	drop constraint fk_veiculo_grupo;

alter table veiculo
	add constraint fk_veiculo_grupo
		foreign key (codigo_grupo)
		references grupo
		on delete set null,
	add constraint fk_veiculo_modelo
		foreign key (codigo_modelo)
		references modelo
		on delete set null;
		
alter table acessorio_veiculo
	drop constraint fk_acessorio_veiculo_veiculo;

alter table acessorio_veiculo
	add constraint fk_acessorio_veiculo_veiculo
		foreign key (id_veiculo)
		references veiculo
		on delete cascade,
	add constraint fk_acessorio_veiculo_acessorio
		foreign key (sigla_acessorio)
		references acessorio
		on update cascade;
		
--		on update restrict
--		on delete restrict

insert into grupo (nome) values 
('Básico'),
('Utilitário'),
('Luxo');

'insert into modelo (nome) values 
('Gol'),
('Celta'),
('Uno');
'
select * 
from grupo;

delete 
from grupo;

-- insert de teste em veiculo, com modelo fixo, pois a tabela correspondente não foi criada ainda.
insert into veiculo (
	placa					,
	ano_fabricacao			,
	data_aquisicao			,
	data_venda				,
	quilometragem			,
	codigo_grupo			,
	codigo_modelo			)
values
(	'SAD4566',2019,'2019-09-15',null, 39000, (select codigo from grupo where nome = 'Luxo'),2),
(	'EFG4555',2019,'2019-07-01',null, 35000, (select codigo from grupo where nome = 'Básico') ,1),
(	'BCD3456',2020,'2020-07-01','2021-08-05', 23000, (select codigo from grupo where nome = 'Luxo') ,1),
(	'ABC1234',2000,'2021-07-01',null, 9000, (select codigo from grupo where nome = 'Luxo') ,1);

insert into  acessorio (sigla,nome) values
('ar','arcondicionado'),
('ve','vidro elétrico'),
('te','trave elétrica');

insert into acessorio_veiculo (id_veiculo, sigla_acessorio) values
( (select id from veiculo where placa = 'ABC1234'),'ar'),
( (select id from veiculo where placa = 'ABC1234'),'ve');

select * 
from veiculo;

select * 
from acessorio_veiculo;

select * 
from acessorio;

update acessorio
set sigla = 'ac'
where sigla = 'ar';

delete
from veiculo
where placa = 'ABC1234';

delete
from grupo
where nome = 'Luxo';

insert into acessorio_veiculo (id_veiculo, sigla_acessorio) values
( (select id from veiculo where placa = 'ABC1234'),'ac'),
( (select id from veiculo where placa = 'ABC1234'),'ve');

delete
from acessorio
where sigla = 've';

-------------------------------------------

select sigla || ' - ' || nome 
from acessorio;

select concat(sigla, ' - ', nome)
from acessorio;

select * 
from veiculo
where quilometragem between 1000 and 10000;

select * 
from veiculo
where quilometragem >= 1000 and 
quilometragem <= 10000;

-- Operadores aritméticos: + - /  *
-- Operadores relacionais: > >= < <= = <> betwwen
-- Operadores lógicos: and or not

select * 
from veiculo
where data_venda is null;

select * 
from veiculo
where data_venda is not null;

select 	placa,
		data_aquisicao,
		coalesce(data_venda::varchar,'(veículo não vendido)') as venda,
		coalesce(cast(data_venda as varchar),'(veículo não vendido)') venda
from	veiculo;		

-- nvl - oracle
-- isnull - sql server
-- coalesce - ansi

select 	placa
from	veiculo
where data_venda is null;		

select *
from veiculo
where placa like 'A_C%';

select *
from veiculo
where placa ilike 'a_c%';

select *
from veiculo
where lower(placa) like lower('a_c%');

select *
from veiculo
where placa = 'ABC1234 ';

select *
from grupo
where nome = 'Luxo ';

select *
from grupo
where nome = 'Luxo';

select *
from grupo
where nome = trim('   Luxo ');

-- ltrim, rtrim, trim, concat, lower, upper, substr, length e position

select	'*'||ltrim('   Claudio Neiva    ')||'*',
		'*'||rtrim('   Claudio Neiva    ')||'*',
		'*'||trim('   Claudio Neiva    ')||'*',
		concat('*','   Claudio Neiva    ','*'),
		'*'||upper('   Claudio Neiva    ')||'*',
		'*'||lower('   Claudio Neiva    ')||'*',
		substr('Claudio Neiva',5,3),
		length('Claudio Neiva'),
		position('dio' in 'Claudio Neiva')

-- distinct

explain select distinct g.nome as nome_grupo
from veiculo v
inner join grupo g on (v.codigo_grupo = g.codigo)
inner join modelo m on (v.codigo_modelo = m.codigo)
where m.nome = 'Gol';

-- Current_date, current_time, current_timestamp;

select current_date, current_time, current_timestamp;

-- Extract(campo from data/hora);

select 	placa,
		data_aquisicao,
		extract( year from current_date) - extract( year from data_aquisicao) as qtd_anos_aquisicao
from veiculo;

-- Funções de agregação: count, max, min, sum, avg;

select 	count(*) as qtd_registros, 
		count(data_venda) as qtd_veiculo_NAO_VENDIDO, 
		count(placa) as qtd_placas, 
		max(quilometragem) as max_quilometragem, 
		min(quilometragem) as min_quilometragem, 
		sum(quilometragem) as sum_quilometragem, 
		avg(quilometragem) as avg_quilometragem
from veiculo;

-- Group by;
select 	codigo_grupo,
		count(placa) as qtd_placas,
		max(quilometragem) as max_quilometragem, 
		min(quilometragem) as min_quilometragem, 
		sum(quilometragem) as sum_quilometragem, 
		avg(quilometragem) as avg_quilometragem
from veiculo
group by codigo_grupo;

-- Order by;
select *
from veiculo
order by ano_fabricacao asc,
		 data_aquisicao desc;

-- Having
-- Listar os grupos de veículo com quimetragem total maior que 70.000km
select 	codigo_grupo,
		sum(quilometragem) as sum_quilometragem
from veiculo
group by codigo_grupo

-- Produto cartesiano - NÃO DEVE FAZER, SE SEU OBJETIVO FOR UNIR AS TABELAS SOB CERTOS CRITÉRIOS!!!!
-- explain 
select 	*
from 	veiculo v,
		grupo g,
		modelo m
where	g.codigo = v.codigo_grupo
and		m.codigo = v.codigo_modelo
and		g.nome = 'Básico';
		
-- 4 veículo x 3 grupos x 3 modelos = 36 registros no resultado

-- JOIN

-- explain 
select 	*
from 		veiculo v
inner join 	grupo g on (g.codigo = v.codigo_grupo)
inner join	modelo m on (m.codigo = v.codigo_modelo)
where	g.nome = 'Básico';


-- JOIN
-- retorne: placa, ano de fabricação, nome do grupo, nome do modelo e nomes dos acessórios de TODOS os veículos cadastrados, mesmo
-- que o veículo não possua nenhum acessório, mas apenas para o grupo LUXO.
select 		v.placa,
			v.ano_fabricacao,
			g.nome as nome_grupo,
			m.nome as nome_modelo,
			coalesce(a.nome,'(Nenhum acessório instalado)') as nome_acessorio
from 		veiculo v 
inner join 	grupo g on (g.codigo = v.codigo_grupo) 
inner join	modelo m on (m.codigo = v.codigo_modelo) 
left outer join acessorio_veiculo av on (av.id_veiculo = v.id) 
left outer join acessorio a on (a.sigla = av.sigla_acessorio)
where		g.nome = 'Luxo';

-- Retorne: id do veículo e nome dos acessórios instalados no veículo. No retorno também devem ser incluídos os nomes de acessório que 
-- não foram instalados em nenhum veículo
select 	av.id_veiculo,
		a.nome as nome_acessorio
from	acessorio_veiculo av 
right outer join acessorio a on (a.sigla = av.sigla_acessorio);

-- Retorne: todos os veículos combinados com todos os acessorios.
select	*
from 		veiculo
cross join	acessorio;

-- Retorne: id do veículo e nome dos acessórios instalados no veículo e os nomes dos acessórios, além dos veículos que não possuem acessórios instalados.
-- No retorno você deve ter: todos os acessóríos, todos os veículos e, quando possível, os acessórios ligados aos veículos.
-- Quando o acessório não estiver instalado em nenhum veículo, mostrar a mensagem: "(não instalado em veículo)";
-- Quando o veículo não estiver nenhum acessório instalado, mostrar a mensagem: "(sem acessório instalado)";

select 	coalesce(v.id :: varchar,'(não instalado em veículo)') as id_veiculo,
		coalesce(a.nome,'(sem acessório instalado)') as nome_acessorio
from	veiculo v
full outer join	acessorio_veiculo av on (av.id_veiculo = v.id)
full outer join acessorio a on (a.sigla = av.sigla_acessorio);

-- 1 - arcondicionado
-- 1 - vidro elétrico
-- (não instalado em veículo) - trava elétrica
-- 2 - (sem acessório instalado)
-- 3 - (sem acessório instalado)
-- 4 - (sem acessório instalado)

-- natural join
-- não foram instalados em nenhum veículo

alter table acessorio rename column sigla to sigla_acessorio;

select 	av.id_veiculo,
		a.nome as nome_acessorio
from	acessorio_veiculo av 
natural join acessorio a;

alter table acessorio rename column sigla_acessorio to sigla;

------- parênteses sobre chave estrangeira

drop table if exists empregado cascade;
drop table if exists dependente cascade;

create table empregado (
	matricula			serial			not null,
	nome				varchar(40)		not null,
	endereco			varchar(255) 	not null,
	telefone			char(11)		not null,
	departamento_id		integer			    null,
	cpf					char(11)		not null,
	constraint pk_empregado
		primary key (matricula),
	constraint un_empregado_cof
		unique (cpf));

create table dependente (
	cpf_empregado		char(11)		not null,
	seq					integer			not null,
	nome				varchar(40)		not null,
	telefone			char(11)		not null,
	constraint pk_dependente
		primary key (cpf_empregado, seq));
		
-- outras tabelas podem ser criadas aqui 		

alter table dependente
	add constraint fk_dependente_empregado
		foreign key (cpf_empregado)
		references empregado (cpf)
		on delete cascade
		on update cascade;

--- exemplo de union

insert into empregado (nome, endereco, telefone, departamento_id, cpf)
values 
('claudio','rua x','71-123',null,'098'),
('antonio','rua y','71-213',null,'987'),
('murillo','rua z','71-345',null,'876'),
('michel' ,'rua w','71-456',null,'765');

insert into dependente (cpf_empregado, seq, nome, telefone)
values 
('098',1,'manuela','71-675'),
('098',2,'mateus','71-456'),
('876',1,'maria','71-876'),
('876',2,'michel','71-456');
-- Lista de telefones para contato, esta lista deve incluir os nomes e telefones, para empregados e dependentes. A lista deve ser composta de duas colunas,
-- uma para nome e outra para o telefone, não sendo necessário indicar a relação entre empregados e seus dependentes.

select	nome,
		telefone
from 	empregado;

select	nome,
		telefone
from 	dependente;

-- NÃO RESOLVE!!!!! NÃO É POSSÍVEL RESOLVER POR JUNÇÃO
select	e.nome as nome_empregado,
		e.telefone as telefone_empregado,
		d.nome as nome_dependente,
		d.telefone as telefone_dependente
from 			empregado e
left outer join dependente d on (d.cpf_empregado = e.cpf);

select	nome,
		telefone
from 	empregado
union
select	nome,
		telefone
from 	dependente
order by 1 asc;

-- ao usar o union, fique atento, pois o mesmo remove linhas duplicadas, mesmo que uma das tabelas
-- quando vc precisar das linhas duplicadas, vc deve utilizar o UNION ALL

pagamento de vale		10,00
pagamento de vale		10,00
pagamento de vale		10,00
pagamento de vale		10,00
energia				  7729,00
água				  2134,00
pagamento de vale		10,00
pagamento de vale		10,00

pagamento de vale		10,00
energia				  7729,00
água				  2134,00

-- Retorne: os nomes e telefones dos empregados que NÃO possuem dependentes.

select * 
from empregado;

select * 
from dependente;

"098        " - tem dependente
"987        " - não tem dependente
"876        " - tem dependente
"765        " - não tem dependente

"antonio"	,"71-213     "
"michel"	,"71-456     "

select	e.nome,
		e.telefone
from	empregado e
where	e.cpf not in (select 	d.cpf_empregado
					 	from	dependente d);

-- Retorne: os nomes e telefones dos empregados que POSSUEM dependentes.

-- NÃO há perda de performance por ser uma subconsulta, pois a consulta interna executa apenas 1 vez.
select	e.nome,
		e.telefone
from	empregado e
where	e.cpf in (select 	d.cpf_empregado
				 	from	dependente d);

-- HÁ perda de performance por ser uma subconsulta, pois a consulta interna executa para cada registro da tabela empregado.
-- Sempre que possível, NÃO utilize campos da tabela de fora na consulta interna, pois isso gera a necessidade de reexecutar a consulta 
-- interna para cada registro da tabela de fora, prejudicando DRÁSTICAMENTE a performance da consulta como um todo.
-- As consultas a seguir NÃO devem ser utilizadas.
select	e.nome,
		e.telefone
from	empregado e
where	(select 	count(*)
		 	from	dependente d
		where		d.cpf_empregado = e.cpf) > 0;

select	e.nome,
		e.telefone
from	empregado e
where	exists (select 	*
				from	dependente d
			where		d.cpf_empregado = e.cpf);

-- View

drop view if exists v_veiculo;

create or replace view v_veiculo 
as
	select 	v.id,
			v.placa,
			v.ano_fabricacao,
			to_char(v.data_aquisicao,'dd/MM/yyyy') as data_aquisicao,
			coalesce( to_char(v.data_venda,'dd/MM/yyyy'),'(não vendido)') as data_venda,
			v.quilometragem,
			g.nome as nome_grupo,
			m.nome as nome_modelo
	from veiculo v
	inner join grupo g on (g.codigo = v.codigo_grupo) 
	inner join modelo m on (m.codigo = v.codigo_modelo) 
;

explain 
select	*
from 	v_veiculo v
left outer join acessorio_veiculo av on (av.id_veiculo = v.id);

-- Materialized View

drop materialized view if exists mv_veiculo;

create materialized view mv_veiculo 
as
	select 	v.id,
			v.placa,
			v.ano_fabricacao,
			to_char(v.data_aquisicao,'dd/MM/yyyy') as data_aquisicao,
			coalesce( to_char(v.data_venda,'dd/MM/yyyy'),'(não vendido)') as data_venda,
			v.quilometragem,
			g.nome as nome_grupo,
			m.nome as nome_modelo
	from veiculo v
	inner join grupo g on (g.codigo = v.codigo_grupo) 
	inner join modelo m on (m.codigo = v.codigo_modelo) 
;

explain 
select	*
from 	mv_veiculo mv
left outer join acessorio_veiculo av on (av.id_veiculo = mv.id);

insert into veiculo (
	placa					,
	ano_fabricacao			,
	data_aquisicao			,
	data_venda				,
	quilometragem			,
	codigo_grupo			,
	codigo_modelo			)
values
(	'KLP2354',2021,'2021-09-20',null, 100, (select codigo from grupo where nome = 'Luxo'),2);

select	*
from	veiculo;

select	*
from	mv_veiculo;

refresh materialized view mv_veiculo;

select	*
from	mv_veiculo;

-- Uma view materializada equivale à um select into, com a vantagem da view materializada possuir o recurso do refresh

select 	v.id,
		v.placa,
		v.ano_fabricacao,
		to_char(v.data_aquisicao,'dd/MM/yyyy') as data_aquisicao,
		coalesce( to_char(v.data_venda,'dd/MM/yyyy'),'(não vendido)') as data_venda,
		v.quilometragem,
		g.nome as nome_grupo,
		m.nome as nome_modelo
into	tab_veiculo_consolidado 	-- aqui seria o nome da view materializada		
from veiculo v
inner join grupo g on (g.codigo = v.codigo_grupo) 
inner join modelo m on (m.codigo = v.codigo_modelo);

explain
select	*
from tab_veiculo_consolidado;
-- Mas NÃO devemos utilizada "select into", uma vez q as views materializadas possuem o recurso de refresh e organizam melhor o modelo do database.


---- Usando arquivos no SO como tabelas no SGBD

CREATE EXTENSION file_fdw;

CREATE SERVER local_file FOREIGN DATA WRAPPER file_fdw;

CREATE FOREIGN TABLE words (word text NOT NULL)
SERVER local_file
OPTIONS (filename '/usr/share/dict/words');

select * 
from words
where word ilike '%brazil%';


----- Motivações para triggers!

-- Atributo derivado?

-- item_nota_fiscal = id_produto, quantidade, valor_unitario, desconto, VALOR_TOTAL
-- o valor_total não precisa ser armazenado, porque ele é derivado dos atributos quantidade, valor_unitario, desconto;
-- motivação para armazenar o VALOR_TOTAL?
select	quantidade * valor_unitario - desconto as valor_total	-- perda de performance, pois a cada execução teríamos o cálculo sendo feito
																-- complexidade das consultas, que deveria reproduzir essa contad, mas isso pode ser
																-- resolvido com o uso de views (não materializadas)
from	item_nota_fiscal .......

create view v_item_nota_fiscal
as
	select	id_produto,
			quantidade,
			valor_unitario,
			desconto,
			quantidade * valor_unitario - desconto as valor_total	
	from 	item_nota_fiscal
;	-- essa view resolve o problema da complexidade, mas não da performance!

-- Caso a opção seja criar o atributo VALOR_TOTAL, esse deve ser mantido (atualizado) pelo próprio sgbd, evitando que ocorra um
-- inconsistência entre o atributos derivado (VALOR_TOTAL) e seus componentes (quantidade, valor_unitario, desconto) / processo
-- de cálculo (quantidade * valor_unitario - desconto).
-- Em outras palavras, esse campo dev ser atualizado automaticamente (via programação no SGBD) quando um registro for atualizado
-- e/ou quando um dos atributos determinantes do valor total for modificado.

insert into item_nota_fiscal (id_produto, quantidade, valor_unitario, desconto, valor_total)
values (...)	-- se vc deixar o programado da camada de aplicação fornecer o valor_total, vc corre o risco de deixar o banco 
				-- em um estado inconsistente.

-- Falar das tabelas de saldo!!!!




				


