create table diaria (
	codigo_grupo			integer			not null,
	data_inicio_vigencia	date			not null,
	data_fim_vigencia		date			not null,
	valor					numeric(10,2)	not null,
constraint pk_diaria
    primary key (codigo_grupo, data_inicio_vigencia),
constraint ck_diaria_vigencia
	check (data_inicio_vigencia <= data_fim_vigencia));
	
-- TODO Unificar os triggers, ao menos para não duplicar a mensagem. 
	
create or replace function f_trg_i_diaria()
returns trigger
as
$$
begin
	if  exists (select	*
		from	diaria
		where	codigo_grupo = new.codigo_grupo
		and		(data_inicio_vigencia, data_fim_vigencia) overlaps (new.data_inicio_vigencia, new.data_fim_vigencia)) then
		raise exception 'A diária não pode ser inserida, pois seu período de vigência se sobrepõe ao de uma diária existente na base de dados.';
	end if;
	return new;
end;
$$
language PLPGSQL;

create or replace function f_trg_u_diaria()
returns trigger
as
$$
begin
	if  exists (select	*
		from	diaria
		where	codigo_grupo = new.codigo_grupo
		and 	data_inicio_vigencia <> old.data_inicio_vigencia
		and		(data_inicio_vigencia, data_fim_vigencia) overlaps (new.data_inicio_vigencia, new.data_fim_vigencia)) then
		raise exception 'A diária não pode ser atualizada, pois seu período de vigência se sobrepõe ao de uma diária existente na base de dados.';
	end if;
	return new;
end;
$$
language PLPGSQL;

drop trigger if exists trg_i_diaria on diaria;
drop trigger if exists trg_u_diaria on diaria;

create trigger trg_i_diaria
before insert
on diaria
for each row
execute procedure f_trg_i_diaria();

create trigger trg_u_diaria
before update of codigo_grupo, data_inicio_vigencia, data_fim_vigencia
on diaria
for each row
execute procedure f_trg_u_diaria();

-- Limpeza da base

delete from diaria;

-- Base	
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-03-01', 'YYYY-MM-DD'),to_date('2021-07-08', 'YYYY-MM-DD'),50.00);

-- Base	
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-01-01', 'YYYY-MM-DD'),to_date('2021-02-01', 'YYYY-MM-DD'),70.00);

-- Falha - 1
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-05-02', 'YYYY-MM-DD'),to_date('2021-06-01', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2021-05-02',
		data_fim_vigencia = '2021-06-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Sucesso - atualização apenas do valor da diária - 1

update 	diaria 
set		valor = 75
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Sucesso - atualização do início da vigência - 1

update 	diaria 
set		data_inicio_vigencia = '2020-12-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Reposição do registro "atualização do início de vigência - 1" para o estado original, viabilizando os próximos testes.

update 	diaria 
set		data_inicio_vigencia = '2021-01-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2020-12-01'
and		data_fim_vigencia = '2021-02-01';

-- Falha - 2
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-05-02', 'YYYY-MM-DD'),to_date('2021-11-09', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2021-05-02',
		data_fim_vigencia = '2021-11-09'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';
		
-- Falha - 3
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-02-01', 'YYYY-MM-DD'),to_date('2021-06-01', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2021-02-01',
		data_fim_vigencia = '2021-06-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';
		
-- Falha - 4
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-02-01', 'YYYY-MM-DD'),to_date('2021-11-09', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2021-02-01',
		data_fim_vigencia = '2021-11-09'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Sucesso - 1
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2020-01-01', 'YYYY-MM-DD'),to_date('2020-02-01', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2019-01-01',
		data_fim_vigencia = '2019-02-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Reset do registro alterado para viabilizar os próximos testes.

update 	diaria 
set		data_inicio_vigencia = '2021-01-01',
		data_fim_vigencia = '2021-02-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2019-01-01'
and		data_fim_vigencia = '2019-02-01';

-- Sucesso - 2
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (10,to_date('2021-10-01', 'YYYY-MM-DD'),to_date('2021-12-01', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		data_inicio_vigencia = '2023-10-01',
		data_fim_vigencia = '2023-12-01'
where	codigo_grupo = 10
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Sucesso - 3
insert into diaria (codigo_grupo, data_inicio_vigencia, data_fim_vigencia, valor) 
values (20,to_date('2021-05-02', 'YYYY-MM-DD'),to_date('2021-06-01', 'YYYY-MM-DD'),70.00);

update 	diaria 
set		codigo_grupo = 30
where	codigo_grupo = 20
and		data_inicio_vigencia = '2021-01-01'
and		data_fim_vigencia = '2021-02-01';

-- Verificar as diárias adicionadas na tabela

select * from diaria;	

-- FIXME Verificar se a atualização simultânea de codigo_grupo e período de vigência é corretamente tratado no 
-- trigger de update. Para tanto, construir os casos de teste.










-- Não ficou legal a tentativa de unificar os triggers de insert e update
-- Essa function ainda NÃO foi testada.
create or replace function f_trg_iu_diaria()
returns trigger
as
$$
declare 
	v_qtd_reg_ins int = 0;
	v_qtd_reg_upd int = 0;
begin
	if (TG_OP = 'UPDATE') then
		select	count(*)
		into	v_qtd_upd
		from	diaria
		where	codigo_grupo = new.codigo_grupo
		and 	data_inicio_vigencia <> old.data_inicio_vigencia
		and		(data_inicio_vigencia, data_fim_vigencia) overlaps (new.data_inicio_vigencia, new.data_fim_vigencia))
	end if;

	if (TG_OP = 'INSERT') then
		select	count(*)
		into	v_qtd_ins
		from	diaria
		where	codigo_grupo = new.codigo_grupo
		and		(data_inicio_vigencia, data_fim_vigencia) overlaps (new.data_inicio_vigencia, new.data_fim_vigencia))
	end if;
	
	if v_qtd_upd > 0 or v_qtd_ins > 0	then
		raise exception 'A diária não pode ser inserida ou atualizada, pois seu período de vigência se sobrepõe ao de uma diária existente na base de dados.';
	end if;
	
	return new;
end;
$$
language PLPGSQL;
