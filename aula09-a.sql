create table aluno (
	matricula		serial			not null,
	nome			varchar(100)	not null,
	constraint pk_aluno
		primary key (matricula));
		
select 	*
from 	aluno;

select nextval('aluno_matricula_seq');

grant select, update, delete, insert
on aluno
to gerente_vendas;

grant usage, update 
on aluno_matricula_seq
to gerente_vendas;

revoke all 
on aluno_matricula_seq
from gerente_vendas;

revoke all
on aluno
from gerente_vendas;

grant select(nome), update, delete, insert
on aluno
to gerente_vendas;

create view v_aluno 
as
	select 	matricula,
			nome
	from	aluno
	where	matricula < 1000
;

select * from v_aluno;

grant select(nome)
on v_aluno
to gerente_vendas;